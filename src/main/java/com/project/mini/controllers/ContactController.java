package com.project.mini.controllers;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import com.project.mini.entities.Customer;
import com.project.mini.service.CustomerService;


@Controller
public class ContactController {
	
	@Autowired
	private CustomerService customerService;
	
	@GetMapping("/customers")
	public String getContact(Map<String, List<Customer>> map,HttpSession session) {
		System.out.println("getContact() is called");
		
		List<Customer> listOfCustomers = this.customerService.getContacts();
		
		if(listOfCustomers == null) {
			System.out.println("Customer fields are empty");
			session.setAttribute("mssg", "List of Customers is empty");
			return "/message";
		}else {
			System.out.println("listOfCustomers is not empty");
			for(Customer customer : listOfCustomers) {
				System.out.println(customer.getEmail());
			}
			map.put("listOfCustomers", listOfCustomers);
			return "customers";
		}
	}
	
	@GetMapping("/update/{email}")
	public String getUpdatePage(@PathVariable String email) {
		System.out.println(email);
		return "/update";
	}
	
	@PostMapping("/doUpdate")
	public String updateDetails(Customer customer,HttpSession session) {
		boolean isUpdated = false;
		if(customer.getEmail().isEmpty() && customer.getFirstName().isEmpty() && customer.getLastName().isEmpty()) {
			System.out.println("Customer fields are empty");
			session.setAttribute("mssg", "Fields can Not be Empty");
			return "/message";
		}else {
			isUpdated = this.customerService.update(customer);
		}
		
		if(isUpdated) {
			System.out.println("Details Updated");
			session.setAttribute("mssg", "Details has been Updated");
			return "/message";
		}else {
			System.out.println("Error in updating");
			session.setAttribute("mssg", "Details has not updated");
			return "/message";
		}
	}
	
	@GetMapping("/delete/{email}")
	public String delete(@PathVariable String email,HttpSession session) {
		System.out.println(email);
		boolean isDeleted = this.customerService.deleteById(email);
		if(isDeleted) {
			session.setAttribute("mssg", "Customer with email "+ email + " is Deleted");
			return "/message";
		}else {
			session.setAttribute("mssg", "Customer with Email " + email + " is not Deleted");
			return "/message";
		}
	}
	
	@GetMapping("/add")
	public String addCustomer() {
		System.out.println("addCustomer() is called");
		return "add";
	}
	
	@PostMapping("/addcustomer")
	public String addNewCustomer(Customer customer,HttpSession session) {
		System.out.println("addCustomer() is called");
		boolean isAdded = false;
		if(customer.getEmail().isEmpty() && customer.getFirstName().isEmpty() && customer.getLastName().isEmpty()) {
			System.out.println("Customer fields are empty");
			session.setAttribute("mssg", "Fields can Not be Empty");
			return "/message";
		}else {
			isAdded = this.customerService.addCustomer(customer);
		}
		
		
		if(isAdded) {
			System.out.println("Customer Added");
			session.setAttribute("mssg", "New Customer with Email " + customer.getEmail() + " is Added");
			return "/message";
		}else {
			System.out.println("Customer is not Added");
			session.setAttribute("mssg", "Customer with Email " + customer.getEmail() + " already Exist");
			return "/message";
		}
	}
}
