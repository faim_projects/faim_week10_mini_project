package com.project.mini.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.project.mini.entities.Customer;
import com.project.mini.repositories.CustomerRepository;


@Service
public class CustomerService {

	@Autowired
	private CustomerRepository customerRepository;

	public List<Customer> getContacts() {
		return (List<Customer>) this.customerRepository.findAll();
	}

	public boolean update(Customer customer) {
		if(!this.customerRepository.existsById(customer.getEmail())) {
			return false;
		}
		this.customerRepository.save(customer);
		return true;
	}

	public boolean deleteById(String email) {
		if(this.customerRepository.existsById(email)) {
			this.customerRepository.deleteById(email);
			return true;
		}
		return false;
	}

	public boolean addCustomer(Customer customer) {
		if(this.customerRepository.existsById(customer.getEmail())) {
			return false;
		}
		this.customerRepository.save(customer);
		return true;
	}

}
