package com.project.mini.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.project.mini.entities.Customer;

@Repository
public interface CustomerRepository extends CrudRepository<Customer,String>{

}
