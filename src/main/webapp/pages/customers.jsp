<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<title>Customer Management</title>
<link
	href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css"
	rel="stylesheet"
	integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3"
	crossorigin="anonymous">
<style>
body {
	background-image: linear-gradient(to right, red, yellow);
}

.header {
	background-color: black;
	color: white;
	height: 100px;
	margin-top: -7px;
	margin-left: -15px;
	width: 101%;
}

.title {
	font-family: Arial, Helvetica, sans-serif;
	display: inline-block;
	text-align: center;
	margin-top: 15px;
	font-size: 40px;
	margin-left: 15px;
	height: 50%;
	padding: 5px;
}

.navLink {
	display: inline-block;
	margin-left: 50%;
}

.navUL {
	list-style-type: none;
}

li {
	display: inline;
}

a {
	color: white;
	text-decoration: none;
}

.li {
	margin-left: 10px;
}

.mark {
	font-size: 40px;
	padding: 5px;
	margin: 10px;
	background-image: linear-gradient(to right, yellow, purple);
	border-radius: 10px;
	color: white;
}

.tbl {
	background-color: green;
	margin-left: 50px;
	margin-right: 50px;
	padding: 10px;
}

.tbl:hover {
	transform: scale(1.01);
}

.trRow {
	width: 70%;
	padding: 10px;
	border: 2px solid black;
}

.tbldiv {
	margin-left: 20px;
	margin-top: 50px;
}

th {
	padding: 50px;
	margin: 10px;
	font-size: 30px;
	text-align: center;
	width: 40%;
	border: 2px solid black;
}

td {
	text-align: center;
	padding: 40px;
	border: 2px solid black;
	font-size: 25px;
	color: white;
}

a {
	text-decoration: underline;
	color: blue;
}

a:hover {
	color: white;
}
</style>
</head>

<body>
	<div class="header" id="headerId">
		<div class="title">Customer Management</div>
		<div class="navLink">
			<ul class="navUL">
				<li class="li"><a href="/customers"><button
							class="btn btn-primary">Customers</button></a></li>
				<li class="li"><a href="add"><button
							class="btn btn-primary">Add Customer</button></a></li>
			</ul>
		</div>
	</div>

	<marquee class="mark" scrollamount="15"> Welcome to Customer
		Management System</marquee>

	<div class="tbldiv">
		<table border="2px" class="tbl">
			<tr class="trRow">
				<th>Email</th>
				<th>First Name</th>
				<th>Last Name</th>
				<th colspan="2">Actions</th>
			</tr>
			<c:forEach items="${listOfCustomers}" var="bookList">
				<tr>
					<td>${bookList.getEmail()}</td>
					<td>${bookList.getFirstName()}</td>
					<td>${bookList.getLastName()}</td>
					<td><a href="update/${bookList.getEmail()}">update</a></td>
					<td><a href="delete/${bookList.getEmail()}">Delete</a></td>
				</tr>
			</c:forEach>
		</table>
	</div>
</body>
</html>