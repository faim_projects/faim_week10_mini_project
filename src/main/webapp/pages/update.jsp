<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Update the Customer Details</title>
<link
	href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css"
	rel="stylesheet"
	integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3"
	crossorigin="anonymous">
<style>
body {
	background-image: linear-gradient(to right, red, yellow);
}

.header {
	background-color: black;
	color: white;
	height: 100px;
	margin-top: -7px;
	margin-left: -15px;
	width: 101%;
}

.title {
	font-family: Arial, Helvetica, sans-serif;
	display: inline-block;
	text-align: center;
	margin-top: 15px;
	font-size: 40px;
	margin-left: 15px;
	height: 50%;
	padding: 5px;
}

.navLink {
	display: inline-block;
	margin-left: 68%;
}

.navUL {
	list-style-type: none;
}

li {
	display: inline;
}

a {
	color: white;
	text-decoration: none;
}

.li {
	margin-left: 10px;
}

.mark {
	font-size: 40px;
	padding: 5px;
	margin: 10px;
	background-image: linear-gradient(to right, yellow, purple);
	border-radius: 10px;
	color: white;
}

.cardLogin {
	margin-left: 30%;
	margin-top: 20px;
	border: 2px solid black;
	display: inline-block;
	width: 35%;
	height: 500px;
	border-radius: 10px;
}

.cardLogin:hover {
	transform: scale(1.10);
	cursor: pointer;
}

.loginTitle {
	background-color: black;
	text-align: center;
	font-size: 30px;
	border-top-left-radius: 8px;
	border-top-right-radius: 8px;
	height: 90px;
	font-size: 50px;
	padding: 5px;
	color: white;
}

.userName {
	margin: 10px;
	padding: 5px;
	font-family: Arial, Helvetica, sans-serif;
}

.inp {
	background-image: linear-gradient(to right, rgb(191, 231, 181),
		rgb(230, 230, 55));
	height: 50px;
	color: black
}

.uname {
	margin-left: 150px;
}
p {
	font-size: 30px;
	margin-right: 100px;
	display: inline-block;
}

.radiobtn {
	font-size: 30px;
}

input[type=radio] {
	height: 20px;
	width: 20px;
}
.lab{
	margin-bottom: 5px;
	font-size: 20px;
}
.name{
	background-color: black;
	color: white;
}
.name:hover{
	background-color: white;
	color: black;
	cursor: pointer;
}

</style>
</head>
<body>
	<div class="header" id="headerId">
		<div class="title">Customer Management</div>
		<div class="navLink">
			<ul class="navUL">
				<li class="li"><a href="/customers"><button
							class="btn btn-primary">Customers</button></a></li>
				<li class="li"><a href="/add"><button
							class="btn btn-primary">Add Customer</button></a></li>
			</ul>
		</div>
	</div>

	<marquee class="mark" scrollamount="15"> Welcome to Customer
		Management System => Update Details</marquee>
		
	<form action="/doUpdate" method="Post">
		<div class="cardLogin">
			<div class="loginTitle">Customer Details</div>
			<div class="form-group userName">
				<label for="id" class="lab">Customer Email:</label> <input type="text"
					class="form-control inp" id="name" value="${email}"
					readonly="readonly" name="email">
			</div>
			<div class="form-group userName">
				<label for="name" class="lab">Customer First Name:</label> <input type="text"
					class="form-control inp" id="name" placeholder="Enter First Name"
					name="firstName">
			</div>
			<div class="form-group userName">
				<label for="genre" class="lab">Customer Last Name:</label> <input type="text"
					class="form-control inp" id="genre" placeholder="Enter Last Name"
					name="lastName">
			</div>
			<div align="center">
				<button type="submit" class="btn btn-success" value="Login">update Details</button>
			</div>
		</div>
	</form>

</body>
</html>